const readline = require('readline');

var name = '';
var age = 0;
// var gender = '';
var income = '';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('What is your name? ', (nameInput) => {
    name = nameInput;
    rl.question('Enter your age? ', (ageInput) => {
        age = ageInput;
        // rl.question('What is your gender? ', (genderInput) => {
        //     gender = genderInput;
        rl.question('Enter your income? ', (incomeInput) => {
            income = incomeInput;
            calculateIncomeTax();
        });
        // });
    });
});


function calculateIncomeTax() {

    var totalIncomeTax = " "

    if (age < 60) {
        if (income < 250000) {
            // console.log("Nil");
            totalIncomeTax = "0"
        }
        else if (income > 250000 && income <= 500000) {
            // console.log("5%");
            totalIncomeTax = income * 0.05
        }
        else if (income > 500000 && income <= 1000000) {
            var exceedingIncom = income - 500000;
            var twentyPercentAmount = exceedingIncom * 0.2;
            totalIncomeTax = 12500 + twentyPercentAmount
        }
        else if (income > 1000000) {
            var exceedingIncom = income - 1000000;
            var thirtyPercentAmount = exceedingIncom * 0.3;
            totalIncomeTax = 112500 + thirtyPercentAmount
        }

    }
    else if (age >= 60 && age <= 80) {
        if (income < 300000) {
            // console.log("Nil");
            totalIncomeTax = "0"

        }
        else if (income > 300000 && income <= 500000) {
            // console.log("5%");
            totalIncomeTax = income * 0.05

        }
        else if (income > 500000 && income <= 1000000) {
            var exceedingIncom = income - 500000;
            var twentyPercentAmount = exceedingIncom * 0.2;
            totalIncomeTax = 10000 + twentyPercentAmount
        }
        else if (income > 1000000) {
            var exceedingIncom = income - 1000000;
            var thirtyPercentAmount = exceedingIncom * 0.3;
            totalIncomeTax = 110000 + thirtyPercentAmount
        }

    }

    else if (age > 80) {
        if (income < 500000) {
            // console.log("Nil");
            totalIncomeTax = "0"
        }
        else if (income > 500000 && income <= 1000000) {
            // console.log("20%");
            totalIncomeTax = income * 0.2
        }
        else if (income > 1000000) {
            var exceedingIncom = income - 1000000;
            var thirtyPercentAmount = exceedingIncom * 0.3;
            totalIncomeTax = 100000 + thirtyPercentAmount
        }

    }

    console.log("Total Income Tax = " + totalIncomeTax);
    rl.close();
}